# FOSSMeet'19 Backend
Clone the repo.   
Setup a MySQL server.   
Create a database.   
Configure database in index.php.   
Run `php -S 127.0.0.1:8000 -t .`   
Go to `http://localhost:8000/admin/init` to initialize tables.   
Go to `http://localhost:8000/admin/login` to login as admin. (Default: admin/admin)   

