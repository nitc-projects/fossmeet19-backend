<?php

class Schedule
{
    public $id;
    public $title;
    public $speaker;
    public $description;
    public $venue;
    public $day_number;
    public $time;
}

class Logic
{

    /**
     * @var mysqli
     */
    private $mysqli_conn;

    public function __construct(
        string $mysql_server,
        string $mysql_username,
        string $mysql_password,
        string $mysql_db,
        int $mysql_port
    ) {
        $this->mysqli_conn = new mysqli($mysql_server, $mysql_username, $mysql_password, $mysql_db, $mysql_port);
    }


    /**
     * Initialize database by setting up tables.
     * Call this function only once after deployment.
     */
    function initialize_database()
    {
        $init_sql = "
            CREATE TABLE IF NOT EXISTS Schedule (
                id INTEGER PRIMARY KEY AUTO_INCREMENT,
                title VARCHAR(255) NOT NULL,
                speaker VARCHAR(255) NOT NULL,
                description VARCHAR(1024) NOT NULL,
                venue VARCHAR(255) NOT NULL,
                day_number INTEGER NOT NULL,
                time CHAR(5) NOT NULL
            );

        ";
        if (!$this->mysqli_conn->query($init_sql)) {
            echo "Can't create table Schedule\n";
            die(var_dump($this->mysqli_conn->error_list));
        }

        $init_sql = "
            CREATE TABLE IF NOT EXISTS Admin (
                id INTEGER PRIMARY KEY AUTO_INCREMENT,
                user_id VARCHAR(100) NOT NULL UNIQUE,
                password VARCHAR(255) NOT NULL
            );
        ";
        if (!$this->mysqli_conn->query($init_sql)) {
            echo "Can't create table Schedule\n";
            die(var_dump($this->mysqli_conn->error_list));
        }

        $this->add_admin('admin', 'admin');
    }

    /**
     * For unit tests.
     */
    function drop_database()
    {
        $drop_sql = "
            DROP TABLE Schedule 
        ";
        if (!$this->mysqli_conn->query($drop_sql)) {
            echo "Can't drop table Schedule\n";
            die(var_dump($this->mysqli_conn->error_list));
        }

        $drop_sql = "
            DROP TABLE Admin
        ";
        if (!$this->mysqli_conn->query($drop_sql)) {
            echo "Can't drop table Admin\n";
            die(var_dump($this->mysqli_conn->error_list));
        }
    }


    function add_admin($user_id, $password)
    {
        $sql = 'INSERT INTO Admin(user_id, password) VALUES(?, ?)';
        $password_hashed = password_hash($password, PASSWORD_DEFAULT);
        if ($prep_stmt = $this->mysqli_conn->prepare($sql)) {
            $prep_stmt->bind_param('ss', $user_id, $password_hashed);
            $prep_stmt->execute();
        } else {
            var_dump($this->mysqli_conn->error_list);
        }
    }

    function admin_exists($user_id, $password) : bool
    {
        $sql = 'SELECT password FROM Admin WHERE user_id=?';
        $prep_stmt = $this->mysqli_conn->prepare($sql);
        $prep_stmt->bind_param('s', $user_id);
        $prep_stmt->execute();
        $prep_stmt->bind_result($result_password);
        $result = $prep_stmt->fetch();
        $prep_stmt->close();
        return $result !== null &&
            password_verify($password, $result_password);
    }


    function update_admin_password($user_id, $new_password)
    {
        $sql = 'UPDATE Admin SET password=? WHERE user_id=?';
        $password_hashed = password_hash($new_password, PASSWORD_DEFAULT);
        $prep_stmt = $this->mysqli_conn->prepare($sql);
        $prep_stmt->bind_param('ss', $password_hashed, $user_id);
        $prep_stmt->execute();
        $prep_stmt->close();
    }

    function delete_admin($user_id)
    {
        $sql = 'DELETE FROM Admin WHERE user_id=?';
        $prep_stmt = $this->mysqli_conn->prepare($sql);
        $prep_stmt->bind_param('s', $user_id);
        $prep_stmt->execute();
        $prep_stmt->close();
    }


    function add_schedule(Schedule $schedule): int
    {
        $sql = 'INSERT INTO Schedule(title, speaker, description, venue, day_number, time) VALUES(?, ?, ?, ?, ?, ?)';
        $prep_stmt = $this->mysqli_conn->prepare($sql);
        if(!$prep_stmt){
            die(var_dump($this->mysqli_conn->error_list));
        } 
        $prep_stmt->bind_param(
            'ssssis',
            $schedule->title,
            $schedule->speaker,
            $schedule->description,
            $schedule->venue,
            $schedule->day_number,
            $schedule->time
        );
        $prep_stmt->execute();
        $prep_stmt->close();
        return $this->mysqli_conn->insert_id;
    }

    function update_schedule(Schedule $schedule)
    {
        $sql = 'UPDATE Schedule SET title=?,speaker=?,description=?,venue=?,day_number=?,time=? WHERE id=?';
        $prep_stmt = $this->mysqli_conn->prepare($sql);
        $prep_stmt->bind_param(
            'ssssisi',
            $schedule->title,
            $schedule->speaker,
            $schedule->description,
            $schedule->venue,
            $schedule->day_number,
            $schedule->time,
            $schedule->id
        );
        $prep_stmt->execute();
        $prep_stmt->close();
    }

    function delete_schedule(int $id)
    {
        $sql = 'DELETE FROM Schedule WHERE id=?';
        $prep_stmt = $this->mysqli_conn->prepare($sql);
        $prep_stmt->bind_param('i', $id);
        $prep_stmt->execute();
        $prep_stmt->close();
    }

    /**
     * @return Schedule[]
     */
    function get_all_schedules()
    {
        $sql_result = $this->mysqli_conn->query('SELECT * FROM Schedule');
        $result = [];
        while ($schedule = $sql_result->fetch_object('Schedule')) {
            $schedule->id = intval($schedule->id);
            $result[] = $schedule;
        }
        return $result;
    }
}