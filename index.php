<?php

require __DIR__ . '/vendor/autoload.php';
require 'logic.php';
require 'session.php';

/*******************************************************************************
 * Initialize 
 *******************************************************************************/

function get_logic() : Logic
{
    $db = 'fossmeet';
    $user = 'fossmeet';
    $password = 'fossmeet';
    $server = '127.0.0.1';
    $port = 3306;
    $logic = new Logic($server, $user, $password, $db, $port);
    return $logic;
}


/*******************************************************************************
 * Routing 
 *******************************************************************************/


/**
 * Used to redirect, and prefix absolute urls in twig templates.
 */
$site_root_dir = dirname($_SERVER['SCRIPT_NAME']);
$request_uri_relative = substr($_SERVER['REQUEST_URI'], strlen($site_root_dir));
if (empty($request_uri_relative) || $request_uri_relative[0] !== '/') {
    /* If hosting at the root folder */
    $request_uri_relative = '/' . $request_uri_relative;
}

$logic = get_logic();
$session = new Session();
session_start();

$twig_loader = new Twig_Loader_Filesystem(__DIR__ . '/templates');
$twig = new Twig_Environment($twig_loader/*, ['cache' => __DIR__ . '/templates_cache']*/);
$twig->addGlobal('site_root_dir', $site_root_dir);

switch ($request_uri_relative) {
    case '/':
        list_schedules($logic, $twig);
        break;

    case '/admin/login':
        admin_login($logic, $session, $twig);
        break;

    case '/admin/logout':
        admin_logout($logic, $session, $twig);
        break;

    case '/admin':
    case '/admin/':
        list_schedules_admin($logic, $session, $twig);
        break;

    case '/admin/schedule_add':
        add_schedule($logic, $session);
        break;

    case '/admin/schedule_delete':
        delete_schedule($logic, $session);
        break;

    case '/admin/schedule_update':
        update_schedule($logic, $session);
        break;

    case '/admin/admin_add':
        add_admin($logic, $session);
        break;

    case '/admin/init':
        if ($_SERVER["REMOTE_ADDR"] == "127.0.0.1") {
            init_db($logic, $session);
        } else {
            page_not_found($twig);
        }
        break;
    default:
        page_not_found($twig);
}

/*******************************************************************************
 * Controllers 
 *******************************************************************************/

function init_db(Logic $logic, Session $session)
{
    $logic->drop_database();
    $logic->initialize_database();
    echo "Database successfully initialized.\n";
}

class ScheduleDay
{
    public $title;
    public $events;
}

class ScheduleEvent
{
    public $title;
    public $speaker;
    public $venue;
    public $description;
    public $time;
}

function list_schedules(Logic $logic, Twig_Environment $twig)
{
    $schedules = $logic->get_all_schedules();
    usort($schedules, function (Schedule $a, Schedule $b) {
        if($a->day_number < $b->day_number) return -1;
        if($a->day_number > $b->day_number) return 1;
        return strcmp($a->time, $b->time);
    });

    $days = [];
    $last_day_number = -1;

    /* @var $schedules Schedule */
    foreach ($schedules as $schedule) {
        if ($schedule->day_number != $last_day_number) {
            $last_day_number = $schedule->day_number;
            $day = new ScheduleDay();
            $day->title = sprintf("Day %d", $schedule->day_number);
            $day->events = [];
            $days[] = $day;
        }

        $event = new ScheduleEvent();
        $event->title = $schedule->title;
        $event->venue = $schedule->venue;
        $event->speaker = $schedule->speaker;
        $event->time = date("g:i A", strtotime($schedule->time));
        $event->description = $schedule->description;

        $last_day = $days[count($days) - 1];
        $last_day->events[] = $event;
    }

    echo $twig->render('schedules.html.twig', ['days' => $days]);
}

function admin_login(Logic $logic, Session $session, Twig_Environment $twig)
{
    if ($session->is_logged_in()) {
        redirect('/admin', false);
    }

    if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
        echo $twig->render('admin_login.html.twig');
        die();
    }

    $user_id = filter_input(INPUT_POST, 'user_id', FILTER_SANITIZE_STRING);
    $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
    if ($logic->admin_exists($user_id, $password)) {
        $session->login($user_id);
        redirect('/admin');
    } else {
        redirect('/admin/login');
    }
}

function admin_logout(Logic $logic, Session $session, Twig_Environment $twig)
{
    $session->logout();
    echo "Logged out successfully.";
}

function list_schedules_admin(Logic $logic, Session $session, Twig_Environment $twig)
{
    if (!$session->is_logged_in()) {
        redirect('/', false);
    }
    $schedules = $logic->get_all_schedules();
    usort($schedules, function (Schedule $a, Schedule $b) {
        if($a->day_number < $b->day_number) return -1;
        if($a->day_number > $b->day_number) return 1;
        return strcmp($a->time, $b->time);
    });

    $days = [];
    $last_day_number = -1;

    /* @var $schedules Schedule */
    foreach ($schedules as $schedule) {
        if ($schedule->day_number != $last_day_number) {
            $last_day_number = $schedule->day_number;
            $day = new ScheduleDay();
            $day->title = sprintf("Day %d", $schedule->day_number);
            $day->events = [];
            $days[] = $day;
        }

        $event = new ScheduleEvent();
        $event->id = $schedule->id;
        $event->title = $schedule->title;
        $event->venue = $schedule->venue;
        $event->speaker = $schedule->speaker;
        $event->time = date("g:i A", strtotime($schedule->time));
        $event->description = $schedule->description;

        $last_day = $days[count($days) - 1];
        $last_day->events[] = $event;
    }

    echo $twig->render('schedules_admin.html.twig', ['days' => $days]);
}


function add_schedule(Logic $logic, Session $session)
{
    if (!$session->is_logged_in()) {
        redirect('/', false);
    }
    if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
        redirect('/admin');
    }
    $schedule = new Schedule();
    $schedule->title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
    $schedule->speaker = filter_input(INPUT_POST, 'speaker', FILTER_SANITIZE_STRING);
    $schedule->venue = filter_input(INPUT_POST, 'venue', FILTER_SANITIZE_STRING);
    $schedule->day_number = intval(filter_input(INPUT_POST, 'day_number', FILTER_SANITIZE_NUMBER_INT));
    $schedule->time = filter_input(INPUT_POST, 'time', FILTER_SANITIZE_STRING);
    $schedule->description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);

    $id = $logic->add_schedule($schedule);

    if (isset($_FILES['speaker_img'])) {
        $src_file = $_FILES['speaker_img']['tmp_name'];
        $target_file = __DIR__ . '/speaker_images/' . $id . '.jpg';
        move_uploaded_file($src_file, $target_file);
    }

    redirect('/admin');
}


function update_schedule(Logic $logic, Session $session)
{
    if (!$session->is_logged_in()) {
        redirect('/', false);
    }
    if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
        redirect('/admin');
    }
    $schedule = new Schedule();
    $schedule->id = intval(filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT));
    $schedule->title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
    $schedule->speaker = filter_input(INPUT_POST, 'speaker', FILTER_SANITIZE_STRING);
    $schedule->venue = filter_input(INPUT_POST, 'venue', FILTER_SANITIZE_STRING);
    $schedule->day_number = filter_input(INPUT_POST, 'day_number', FILTER_SANITIZE_STRING);
    $schedule->time = filter_input(INPUT_POST, 'time', FILTER_SANITIZE_STRING);
    $schedule->description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);

    $id = $schedule->id;

    if (isset($_FILES['speaker_img'])) {
        $src_file = $_FILES['speaker_img']['tmp_name'];
        $target_file = 'speaker_images/' . $id . '.jpg';
        move_uploaded_file($src_file, $target_file);
    }

    redirect('/admin');
}


function delete_schedule(Logic $logic, Session $session)
{
    if (!$session->is_logged_in()) {
        redirect('/', false);
    }
    if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
        redirect('/admin');
    }
    $id = intval(filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT));

    $logic->delete_schedule($id);
    redirect('/admin');
}

function add_admin(Logic $logic, Session $session)
{
    if (!$session->is_logged_in()) {
        redirect('/', false);
    }
    if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
        redirect('/admin');
    }
    $user_id = filter_input(INPUT_POST, 'user_id', FILTER_SANITIZE_STRING);
    $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
    $logic->add_admin($user_id, $password);

    redirect('/admin');
}

function update_admin_password(Logic $logic, Session $session)
{
    if (!$session->is_logged_in()) {
        redirect('/', false);
    }
    if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
        redirect('/admin');
    }
    $user_id = filter_input(INPUT_POST, 'user_id', FILTER_SANITIZE_STRING);
    $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
    $logic->update_admin_password($user_id, $password);

    redirect('/admin');
}

function page_not_found()
{
    echo "<h1>404: Page not found</h1>";
}

/*******************************************************************************
 * Helpers 
 *******************************************************************************/

function redirect($url, $permanent = false)
{
    global $site_root_dir;
    $fixed_url = $site_root_dir.$url;
    header('Location: ' . $fixed_url, true, $permanent ? 301 : 302);
    exit();
}