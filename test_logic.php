<?php
require __DIR__ . '/vendor/autoload.php';
include 'logic.php';

use PHPUnit\Framework\TestCase;

/**
 * Integration testing for logic.php
 * Please setup a test db for running these tests.
 */

final class LogicTest extends TestCase
{
    private function get_logic() : Logic
    {
        $test_db = 'fossmeet_test';
        $test_user = 'fossmeet';
        $test_password = 'fossmeet';
        $test_server = '127.0.0.1';
        $test_port = 3306;
        $logic = new Logic($test_server, $test_user, $test_password, $test_db, $test_port);
        $logic->initialize_database();
        $logic->drop_database();
        $logic->initialize_database();
        return $logic;
    }


    public function test_admin() : void
    {
        $logic = $this->get_logic();

        $logic->delete_admin('test');

        $this->assertTrue(!$logic->admin_exists('test', 'test123'), "Admin exists failed.");
        echo "Admin not exists success\n";

        $logic->add_admin('test', 'test123');
        $this->assertTrue($logic->admin_exists('test', 'test123'), "Admin insertion or retrieval failed.");
        echo "Admin insertion success\n";
        echo "Admin exists success\n";

        $logic->update_admin_password('test', 'asdfg');
        $this->assertTrue($logic->admin_exists('test', 'asdfg'), "Admin update failed");
        echo "Admin update password success\n";

        $logic->delete_admin('test');
        $this->assertTrue(!$logic->admin_exists('test', 'asdfg'), "Admin deletion failed.");
        echo "Admin delete success\n";
    }

    function test_schedules()
    {
        $logic = $this->get_logic();

        $result_schedules = $logic->get_all_schedules();
        $this->assertEquals(count($result_schedules), 0, "Schedule initial get_all not empty");
        echo "Schedule empty get_all success.\n";

        $s = new Schedule();
        $s->title = "Test Title";
        $s->speaker = "Test Speaker";
        $s->venue = "Test Venue";
        $s->description = "Test Description";
        $s->day_number = 1;
        $s->time = "19:00";

        $s->id = $logic->add_schedule($s);

        $result_schedules = $logic->get_all_schedules();
        $this->assertEquals(count($result_schedules), 1, "Schedule insertion count mismatch");

        $s1 = $result_schedules[0];
        $this->assertEquals($s, $s1, "Schedule inserted data doesn't match");

        echo "Schedule insert success.\n";

        $s1->title = "Updated title";
        $s1->speaker = "Updated speaker";
        $s1->description = "Updated Description";
        $s1->venue = "Updated venue";
        $s->day_number = 5;
        $s1->time = "14:00";

        $logic->update_schedule($s1);

        $result_schedules = $logic->get_all_schedules();
        $this->assertEquals(count($result_schedules), 1, "Schedule retrieval count mismatch");
        $s2 = $result_schedules[0];
        $this->assertEquals($s1, $s2, "Schedule updated data doesn't match");

        echo "Schedule update success.\n";

        $logic->delete_schedule($s2->id);
        $result_schedules = $logic->get_all_schedules();
        $this->assertEquals(count($result_schedules), 0, "Schedule delete failed");

        echo "Schedule delete success.\n";
    }

}