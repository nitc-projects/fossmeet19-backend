<?php

class Session {
    /**
     * 
     *  $_SESSION['Session__logged_in_user']
     *      Currently logged in user
     *      string| NULL
     * 
     */

    function login($user) {
        $_SESSION['Session__logged_in_user'] = $user;
    }

    function is_logged_in() {
        return isset($_SESSION['Session__logged_in_user']);
    }

    function logout() {
        unset($_SESSION['Session__logged_in_user']);
    }
}